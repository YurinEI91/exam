//
//  main.cpp
//  exam
//
//  Created by Eugene Yurin on 16.05.2020.
//  Copyright © 2020 Eugene Yurin. All rights reserved.
//

#define MAX_SIZE (100)

#include <fstream>
#include <vector>
#include <random>

using namespace std;

vector<string> name;
vector<unsigned> num;
random_device rd;
mt19937 g(rd());

int open() {
    ifstream i_file("input.txt");
    if (!i_file.is_open())
        return 1;
    
    char buffer [MAX_SIZE];
    while(i_file.getline(buffer, MAX_SIZE))
        name.push_back(buffer);
    i_file.close();
    if (name.size() < 2)
        return 2;
    name.shrink_to_fit();
    for (auto &n: name)
        if (n.find('\r')!=n.npos)
            n.erase(n.find('\r'));
    num.resize(name.size());
    for (unsigned i = 0; i < num.size(); ++i)
        num[i] = i + 1;
    return 0;
}

int main(int argc, const char * argv[]) {
    setlocale(LC_ALL, "rus");
    if (open())
        return 1;
    
    ofstream I_file("I.txt");
    ofstream II_file("II.txt");

    shuffle(num.begin(), num.end(), g);
    for(unsigned i = 0; i < name.size(); ++i)
        name[i] += '\t' + to_string(num[i]);
    

    shuffle(num.begin(), num.end(), g);
    for(unsigned i = 0; i < name.size(); ++i)
        if (num[i] % 2)
            I_file << name[i] << "\r\n";
        else
            II_file << name[i] << "\r\n";
    
    I_file.close();
    II_file.close();
    
    return 0;
}